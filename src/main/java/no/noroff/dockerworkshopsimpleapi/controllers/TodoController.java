package no.noroff.dockerworkshopsimpleapi.controllers;

import no.noroff.dockerworkshopsimpleapi.models.Todo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class TodoController {

    private final String message;

    public TodoController( @Value("${message}") String message) {
        this.message = message;
    }

    @GetMapping("todos")
    public ResponseEntity<List<Todo>> getAll() {
        List<Todo> todos = Arrays.asList(
                new Todo(1, "Make breakfast"),
                new Todo(2, "Wash dishes"),
                new Todo(3, "Walk dog"),
                new Todo(4, "Walk cat")
        );
        return ResponseEntity.ok(todos);
    }

    @GetMapping("env")
    public ResponseEntity getEnv() {
        return ResponseEntity.ok(message);
    }
}
